﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace RPE.BO
{
    public static class IGRPE
    {
        public static Image ByteArrayToImage(byte[] byteArrayIn)
        {
            using (MemoryStream ms = new MemoryStream(byteArrayIn))
            {
                Image returnImage = Image.FromStream(ms);
                return returnImage;
            }
        }

        public static Image CarregarFoto(Usuario usuario)
        {
            Image imagem;

            if (usuario.FOTO == null)
            {
                return imagem = null;
            }
            else
            {
                return imagem = IGRPE.ByteArrayToImage(usuario.FOTO.ToArray());
            }

        }// Fim do Método

        public static bool RegistrarPonto(Usuario usuario)
        {
            try
            {
                IGRPE.LOG(usuario);
                IGRPE.VerificarRegistro(usuario);
                
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static DateTime GetDate()
        {
            der_db01Entities bd = new der_db01Entities();
            DateTime horario = bd.Database.SqlQuery<DateTime>("SELECT GETDATE()").First();

            // horario = horario.AddHours(1); // acrecenta uma hora para brasília

            return horario;
        }
       
        public static void LOG(Usuario usuario)
        {
            der_db01Entities bd = new der_db01Entities();
            DateTime data = GetDate(); // pega a data e hora do servidor

            Log log = new Log();
            log.USUARIO_ID = usuario.USUARIO_ID;
            log.DATA_HORA = DateTime.Now;
            
            bd.Logs.Add(log);
            bd.SaveChanges();

        }

        public static void AtualizarHorario(Usuario usuario, Frequencia _frequencia, DateTime data)
        {
            der_db01Entities bd = new der_db01Entities();
            Frequencia frequencia = bd.Frequencias.SingleOrDefault(c => c.FREQUENCIA_ID == _frequencia.FREQUENCIA_ID);
            try
            {
                frequencia.ULTIMO_REGISTRO = data.Hour + ":" + data.Minute;

                if (frequencia.SAIDA_MANHA == null)
                {
                    if (!Tolerancia(frequencia.ENTRADA_MANHA.ToString(), data))
                    {
                        throw new Exception("Aguarde pelo menos 1 hora, sua entrada na manha foi as " + frequencia.ENTRADA_MANHA);
                    }
                    frequencia.TIPO = "P";
                    frequencia.SAIDA_MANHA = TimeSpan.Parse(data.ToString("HH:mm:ss"));
                    
                    bd.SaveChanges();
                    //return;
                }
                else if (frequencia.ENTRADA_TARDE == null)
                {
                    if (!Tolerancia(frequencia.SAIDA_MANHA.ToString(), data))
                    {
                        throw new Exception("Aguarde pelo menos 1 hora, sua saida de manha foi as " + frequencia.SAIDA_MANHA);
                    }
                    frequencia.TIPO = "P";
                    frequencia.ENTRADA_TARDE = TimeSpan.Parse(data.ToString("HH:mm:ss"));
                    bd.SaveChanges();
                    //return;
                }
                else
                {
                    if (!Tolerancia(frequencia.ENTRADA_TARDE.ToString(), data))
                    {
                        throw new Exception("Aguarde pelo menos 1 hora, sua entrada a tarde foi as " + frequencia.ENTRADA_TARDE);
                    }
                    frequencia.TIPO = "P";
                    frequencia.SAIDA_TARDE = TimeSpan.Parse(data.ToString("HH:mm:ss"));
                    bd.SaveChanges();
                   // return;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void RegistraHorario(Usuario usuario, DateTime data)
        {
            der_db01Entities bd = new der_db01Entities();

            System.Globalization.CultureInfo ciBrazil = new System.Globalization.CultureInfo("pt-BR");
            System.Globalization.DateTimeFormatInfo dtfi = ciBrazil.DateTimeFormat;

            Frequencia frequencia = new Frequencia();

            frequencia.ENTRADA_MANHA = TimeSpan.Parse(data.ToString("HH:mm:ss"));
            frequencia.DATA = data.Date.ToString("dd/MM/yyyy");
            frequencia.USUARIO_ID = usuario.USUARIO_ID;
            frequencia.ULTIMO_REGISTRO = data.Hour + ":" + data.Minute;
            frequencia.TIPO = "P";
            frequencia.DIA_SEMANA = dtfi.GetDayName(data.DayOfWeek);

            bd.Frequencias.Add(frequencia);
            bd.SaveChanges();
        }

        public static void VerificarRegistro(Usuario usuario)
        {
            der_db01Entities bd = new der_db01Entities();

            DateTime data = GetDate();
            string data_hoje = data.ToShortDateString();
            Frequencia frequencia = bd.Frequencias.SingleOrDefault(c => c.USUARIO_ID == usuario.USUARIO_ID && c.DATA == data_hoje);

            if (frequencia != null)
            {
                IGRPE.AtualizarHorario(usuario, frequencia, data);
            }
            else
            {
                IGRPE.RegistraHorario(usuario, data);
            }
        }

        public static Boolean Tolerancia(string HORA, DateTime horaFinal)
        {
            DateTime horaInicial = Convert.ToDateTime(HORA);

            TimeSpan tempoPercorrido = horaFinal.Subtract(horaInicial);
            if (tempoPercorrido.Hours > 0 || tempoPercorrido.Minutes > 29)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
    }
    
}
