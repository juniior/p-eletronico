﻿namespace RPE
{
    partial class FRPE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRPE));
            this.lblBancoHoras = new System.Windows.Forms.Label();
            this.lblMatriculaServidor = new System.Windows.Forms.Label();
            this.lblEntradaTarde = new System.Windows.Forms.Label();
            this.lblSaidaManha = new System.Windows.Forms.Label();
            this.lblNomeServidor = new System.Windows.Forms.Label();
            this.lblSaidaTarde = new System.Windows.Forms.Label();
            this.lblRelogio = new System.Windows.Forms.Label();
            this.lblData = new System.Windows.Forms.Label();
            this.Timer_Limpesa = new System.Windows.Forms.Timer(this.components);
            this.lblEntradaManha = new System.Windows.Forms.Label();
            this.Timer_Inicio = new System.Windows.Forms.Timer(this.components);
            this.statusBarPanel1 = new System.Windows.Forms.StatusBarPanel();
            this.labStatus = new System.Windows.Forms.StatusBarPanel();
            this.StatusBar = new System.Windows.Forms.StatusBar();
            this.lblErro = new System.Windows.Forms.Label();
            this.lblSenha = new System.Windows.Forms.Label();
            this.txtSenhaUsuario = new System.Windows.Forms.TextBox();
            this.tAtualizaListaBios = new System.Windows.Forms.Timer(this.components);
            this.pictureExtWnd = new System.Windows.Forms.PictureBox();
            this.fotoServidor = new System.Windows.Forms.PictureBox();
            this.picBackGround = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureExtWnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fotoServidor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBackGround)).BeginInit();
            this.SuspendLayout();
            // 
            // lblBancoHoras
            // 
            this.lblBancoHoras.AutoSize = true;
            this.lblBancoHoras.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBancoHoras.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblBancoHoras.Location = new System.Drawing.Point(63, 492);
            this.lblBancoHoras.Name = "lblBancoHoras";
            this.lblBancoHoras.Size = new System.Drawing.Size(0, 37);
            this.lblBancoHoras.TabIndex = 52;
            // 
            // lblMatriculaServidor
            // 
            this.lblMatriculaServidor.AutoSize = true;
            this.lblMatriculaServidor.BackColor = System.Drawing.Color.Transparent;
            this.lblMatriculaServidor.Font = new System.Drawing.Font("Arial", 39.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMatriculaServidor.ForeColor = System.Drawing.Color.DarkBlue;
            this.lblMatriculaServidor.Location = new System.Drawing.Point(290, 41);
            this.lblMatriculaServidor.Name = "lblMatriculaServidor";
            this.lblMatriculaServidor.Size = new System.Drawing.Size(286, 60);
            this.lblMatriculaServidor.TabIndex = 50;
            this.lblMatriculaServidor.Text = "300106080";
            // 
            // lblEntradaTarde
            // 
            this.lblEntradaTarde.AutoSize = true;
            this.lblEntradaTarde.BackColor = System.Drawing.Color.Transparent;
            this.lblEntradaTarde.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEntradaTarde.ForeColor = System.Drawing.Color.MediumBlue;
            this.lblEntradaTarde.Location = new System.Drawing.Point(626, 238);
            this.lblEntradaTarde.Name = "lblEntradaTarde";
            this.lblEntradaTarde.Size = new System.Drawing.Size(71, 27);
            this.lblEntradaTarde.TabIndex = 47;
            this.lblEntradaTarde.Text = "14:00";
            this.lblEntradaTarde.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSaidaManha
            // 
            this.lblSaidaManha.AutoSize = true;
            this.lblSaidaManha.BackColor = System.Drawing.Color.Transparent;
            this.lblSaidaManha.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSaidaManha.ForeColor = System.Drawing.Color.MediumBlue;
            this.lblSaidaManha.Location = new System.Drawing.Point(389, 277);
            this.lblSaidaManha.Name = "lblSaidaManha";
            this.lblSaidaManha.Size = new System.Drawing.Size(71, 27);
            this.lblSaidaManha.TabIndex = 46;
            this.lblSaidaManha.Text = "12:00";
            this.lblSaidaManha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNomeServidor
            // 
            this.lblNomeServidor.AutoSize = true;
            this.lblNomeServidor.BackColor = System.Drawing.Color.Transparent;
            this.lblNomeServidor.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeServidor.ForeColor = System.Drawing.Color.Black;
            this.lblNomeServidor.Location = new System.Drawing.Point(291, 116);
            this.lblNomeServidor.Name = "lblNomeServidor";
            this.lblNomeServidor.Size = new System.Drawing.Size(436, 32);
            this.lblNomeServidor.TabIndex = 49;
            this.lblNomeServidor.Text = "OSVALDO DOS SANTOS JUNIOR";
            // 
            // lblSaidaTarde
            // 
            this.lblSaidaTarde.AutoSize = true;
            this.lblSaidaTarde.BackColor = System.Drawing.Color.Transparent;
            this.lblSaidaTarde.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSaidaTarde.ForeColor = System.Drawing.Color.MediumBlue;
            this.lblSaidaTarde.Location = new System.Drawing.Point(626, 277);
            this.lblSaidaTarde.Name = "lblSaidaTarde";
            this.lblSaidaTarde.Size = new System.Drawing.Size(71, 27);
            this.lblSaidaTarde.TabIndex = 48;
            this.lblSaidaTarde.Text = "18:00";
            this.lblSaidaTarde.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRelogio
            // 
            this.lblRelogio.BackColor = System.Drawing.Color.Transparent;
            this.lblRelogio.Font = new System.Drawing.Font("Arial", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRelogio.ForeColor = System.Drawing.Color.White;
            this.lblRelogio.Location = new System.Drawing.Point(449, 348);
            this.lblRelogio.Name = "lblRelogio";
            this.lblRelogio.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblRelogio.Size = new System.Drawing.Size(476, 148);
            this.lblRelogio.TabIndex = 45;
            this.lblRelogio.Text = "00:00:00";
            this.lblRelogio.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblData
            // 
            this.lblData.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblData.Location = new System.Drawing.Point(287, 561);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(618, 23);
            this.lblData.TabIndex = 44;
            this.lblData.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Timer_Limpesa
            // 
            this.Timer_Limpesa.Interval = 2000;
            this.Timer_Limpesa.Tick += new System.EventHandler(this.Timer_Limpesa_Tick);
            // 
            // lblEntradaManha
            // 
            this.lblEntradaManha.AutoSize = true;
            this.lblEntradaManha.BackColor = System.Drawing.Color.Transparent;
            this.lblEntradaManha.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEntradaManha.ForeColor = System.Drawing.Color.MediumBlue;
            this.lblEntradaManha.Location = new System.Drawing.Point(389, 238);
            this.lblEntradaManha.Name = "lblEntradaManha";
            this.lblEntradaManha.Size = new System.Drawing.Size(71, 27);
            this.lblEntradaManha.TabIndex = 42;
            this.lblEntradaManha.Text = "08:00";
            this.lblEntradaManha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Timer_Inicio
            // 
            this.Timer_Inicio.Enabled = true;
            this.Timer_Inicio.Tick += new System.EventHandler(this.Timer_Inicio_Tick);
            // 
            // statusBarPanel1
            // 
            this.statusBarPanel1.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            this.statusBarPanel1.Name = "statusBarPanel1";
            this.statusBarPanel1.Width = 780;
            // 
            // labStatus
            // 
            this.labStatus.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents;
            this.labStatus.Name = "labStatus";
            this.labStatus.Text = "Status bar aki";
            this.labStatus.Width = 134;
            // 
            // StatusBar
            // 
            this.StatusBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusBar.Location = new System.Drawing.Point(0, 712);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
            this.labStatus,
            this.statusBarPanel1});
            this.StatusBar.ShowPanels = true;
            this.StatusBar.Size = new System.Drawing.Size(930, 36);
            this.StatusBar.TabIndex = 41;
            // 
            // lblErro
            // 
            this.lblErro.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErro.Location = new System.Drawing.Point(12, 615);
            this.lblErro.Name = "lblErro";
            this.lblErro.Size = new System.Drawing.Size(893, 79);
            this.lblErro.TabIndex = 53;
            this.lblErro.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSenha
            // 
            this.lblSenha.AutoSize = true;
            this.lblSenha.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSenha.ForeColor = System.Drawing.Color.White;
            this.lblSenha.Location = new System.Drawing.Point(63, 419);
            this.lblSenha.Name = "lblSenha";
            this.lblSenha.Size = new System.Drawing.Size(121, 39);
            this.lblSenha.TabIndex = 55;
            this.lblSenha.Text = "Senha";
            // 
            // txtSenhaUsuario
            // 
            this.txtSenhaUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 45F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSenhaUsuario.Location = new System.Drawing.Point(47, 473);
            this.txtSenhaUsuario.MaxLength = 4;
            this.txtSenhaUsuario.Name = "txtSenhaUsuario";
            this.txtSenhaUsuario.PasswordChar = '*';
            this.txtSenhaUsuario.Size = new System.Drawing.Size(153, 75);
            this.txtSenhaUsuario.TabIndex = 54;
            this.txtSenhaUsuario.Text = "1234";
            this.txtSenhaUsuario.UseSystemPasswordChar = true;
            this.txtSenhaUsuario.TextChanged += new System.EventHandler(this.txtSenhaUsuario_TextChanged);
            this.txtSenhaUsuario.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSenhaUsuario_KeyUp);
            // 
            // tAtualizaListaBios
            // 
            this.tAtualizaListaBios.Enabled = true;
            this.tAtualizaListaBios.Interval = 3000000;
            this.tAtualizaListaBios.Tick += new System.EventHandler(this.tAtualizaListaBios_Tick);
            // 
            // pictureExtWnd
            // 
            this.pictureExtWnd.BackColor = System.Drawing.SystemColors.Window;
            this.pictureExtWnd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureExtWnd.Location = new System.Drawing.Point(70, 292);
            this.pictureExtWnd.Name = "pictureExtWnd";
            this.pictureExtWnd.Size = new System.Drawing.Size(104, 112);
            this.pictureExtWnd.TabIndex = 51;
            this.pictureExtWnd.TabStop = false;
            // 
            // fotoServidor
            // 
            this.fotoServidor.Location = new System.Drawing.Point(20, 12);
            this.fotoServidor.Name = "fotoServidor";
            this.fotoServidor.Size = new System.Drawing.Size(227, 250);
            this.fotoServidor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fotoServidor.TabIndex = 43;
            this.fotoServidor.TabStop = false;
            // 
            // picBackGround
            // 
            this.picBackGround.BackColor = System.Drawing.SystemColors.Desktop;
            this.picBackGround.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picBackGround.Image = global::RPE.Properties.Resources.BackGround;
            this.picBackGround.InitialImage = null;
            this.picBackGround.Location = new System.Drawing.Point(0, 0);
            this.picBackGround.Name = "picBackGround";
            this.picBackGround.Size = new System.Drawing.Size(930, 748);
            this.picBackGround.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBackGround.TabIndex = 40;
            this.picBackGround.TabStop = false;
            // 
            // FRPE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(930, 748);
            this.Controls.Add(this.lblSenha);
            this.Controls.Add(this.txtSenhaUsuario);
            this.Controls.Add(this.lblErro);
            this.Controls.Add(this.lblBancoHoras);
            this.Controls.Add(this.pictureExtWnd);
            this.Controls.Add(this.lblMatriculaServidor);
            this.Controls.Add(this.lblEntradaTarde);
            this.Controls.Add(this.lblSaidaManha);
            this.Controls.Add(this.lblNomeServidor);
            this.Controls.Add(this.lblSaidaTarde);
            this.Controls.Add(this.lblRelogio);
            this.Controls.Add(this.lblData);
            this.Controls.Add(this.fotoServidor);
            this.Controls.Add(this.lblEntradaManha);
            this.Controls.Add(this.StatusBar);
            this.Controls.Add(this.picBackGround);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FRPE";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FRPE";
            this.Load += new System.EventHandler(this.FRPE_Load);
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureExtWnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fotoServidor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBackGround)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBancoHoras;
        private System.Windows.Forms.PictureBox pictureExtWnd;
        private System.Windows.Forms.Label lblMatriculaServidor;
        private System.Windows.Forms.Label lblEntradaTarde;
        private System.Windows.Forms.Label lblSaidaManha;
        private System.Windows.Forms.Label lblNomeServidor;
        private System.Windows.Forms.Label lblSaidaTarde;
        private System.Windows.Forms.Label lblRelogio;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.Timer Timer_Limpesa;
        private System.Windows.Forms.PictureBox fotoServidor;
        private System.Windows.Forms.Label lblEntradaManha;
        private System.Windows.Forms.Timer Timer_Inicio;
        private System.Windows.Forms.PictureBox picBackGround;
        private System.Windows.Forms.StatusBarPanel statusBarPanel1;
        private System.Windows.Forms.StatusBarPanel labStatus;
        private System.Windows.Forms.StatusBar StatusBar;
        private System.Windows.Forms.Label lblErro;
        private System.Windows.Forms.Label lblSenha;
        private System.Windows.Forms.TextBox txtSenhaUsuario;
        private System.Windows.Forms.Timer tAtualizaListaBios;
    }
}