﻿using NBioBSPCOMLib;
using NITGEN.SDK.NBioBSP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace RPE
{
    public partial class FRPE : Form
    {
        #region Atributos e Propriedades

        public static der_db01Entities bd = new der_db01Entities();
        IList<Bio> bios;
        System.Globalization.CultureInfo ciBrazil = new System.Globalization.CultureInfo("pt-BR");

        NBioBSP objNBioBSP;
        IDevice objDevice;
        IExtraction objExtraction;
        IMatching objMatching;
        IFPData objFPData;

        NBioAPI m_NBioAPI;
        NBioAPI.Type.WINDOW_OPTION m_WinOption;

        #endregion

        #region Eventos

        public FRPE()
        {
            InitializeComponent();
        }

        private void Timer_Limpesa_Tick(object sender, EventArgs e)
        {
            LimparTela();
        }

        private void FRPE_Load(object sender, EventArgs e)
        {

            txtSenhaUsuario.Text = string.Empty;
            lblSenha.Visible = txtSenhaUsuario.Visible = false;

            lblRelogio.Parent = picBackGround;
            lblBancoHoras.Text = lblData.Text = lblEntradaManha.Text = lblEntradaTarde.Text = lblErro.Text =
                lblMatriculaServidor.Text = lblNomeServidor.Text = lblRelogio.Text = lblSaidaManha.Text = lblSaidaTarde.Text = string.Empty;
            try
            {
                // Create NBioBSP object
                objNBioBSP = new NBioBSPCOMLib.NBioBSP();

                objDevice = (IDevice)objNBioBSP.Device;
                objMatching = (IMatching)objNBioBSP.Matching;
                objFPData = (IFPData)objNBioBSP.FPData;
                objExtraction = (IExtraction)objNBioBSP.Extraction;

                m_NBioAPI = new NBioAPI();
                m_WinOption = new NBioAPI.Type.WINDOW_OPTION();
                m_WinOption.Option2 = new NBioAPI.Type.WINDOW_OPTION_2();

                objNBioBSP.SetSkinResource(".\\NBSP2POR.dll");
                objDevice.Enumerate();// Enumerate Device
                labStatus.Text = objDevice.ErrorDescription + " [" + objDevice.ErrorCode + "]";
                int iDeviceID = NBioBSPType.DEVICE_ID.AUTO;

                objDevice.Close(objDevice.OpenedDeviceID);// Close Device if before opened
                objDevice.Open(iDeviceID);// Open device

                if (objDevice.ErrorCode == NBioBSPError.NONE) labStatus.Text = "Leitor Ok!";
                else labStatus.Text = "Leitor com problemas! - " + objDevice.ErrorDescription + " [" + objExtraction.ErrorCode + "]";

                string[] secretarias = System.Configuration.ConfigurationSettings.AppSettings["secretarias"].Split(';');

                int[] idSecretarias = new int[secretarias.Count()];

                for (int i = 0; i < secretarias.Count(); i++)
                {
                    idSecretarias[i] = int.Parse(secretarias[i]);
                }


                if (idSecretarias.Count() == 1 && idSecretarias[0] == 0)
                {
                    bios = bd.Bios.Where(c => c.Usuario.ATIVO == true).ToList();
                }
                else
                {
                    bios = bd.Bios.Where(c => c.Usuario.ATIVO == true && idSecretarias.Contains(c.Usuario.ORGAO_ID.Value)).ToList();
                }

            }
            catch (Exception ex) { lblErro.Text = ex.Message; }
        }

        private void Timer_Inicio_Tick(object sender, EventArgs e)
        {
            try
            {
                DateTime data = BO.IGRPE.GetDate(); // pega a data e hora do servidor

                lblData.Text = "Ponto DER - " + data.ToShortTimeString() + " - " + data.ToLongDateString();
                //lblRelogio.Text = data.ToShortTimeString();
                lblRelogio.Text = data.ToLongTimeString();

                Timer_Inicio.Interval = 100;

                objDevice.Open(NBioBSPType.DEVICE_ID.AUTO);

                objExtraction.WindowStyle = NBioBSPType.WINDOW_STYLE.INVISIBLE; //Captura sem Pop-up

                // Verifica se existe um dedo posicionado no sensor
                if (objDevice.CheckFinger != 0)
                {
                    objExtraction.DefaultTimeout = 1000;//Tempo de captura = 1 segundo
                    ImpressaoDigital();
                    CapturarDigital();
                }
            }
            catch (Exception ex)
            {
                lblErro.Text = ex.Message;
                lblErro.ForeColor = System.Drawing.Color.Red;
            }
        }

        private void txtSenhaUsuario_TextChanged(object sender, EventArgs e)
        {
            if (txtSenhaUsuario.Text.Count() == 4)
            {
                Usuario _usuario = bd.Usuarios.SingleOrDefault(c => c.MATRICULA == lblMatriculaServidor.Text);

                if (txtSenhaUsuario.Text == _usuario.MATRICULA.Remove(0, 5))
                {
                    if (BO.IGRPE.RegistrarPonto(_usuario))
                    {
                        // Grava ou Atualiza
                        lblErro.Text = "Registrado com Sucesso!!";
                        lblErro.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        lblErro.Text = "Aguarde o 1 hora para registrar novamente!";
                        lblErro.ForeColor = System.Drawing.Color.Red;
                        // Tolerancia não aceita
                    }

                    CarregaHorarios(_usuario);

                    txtSenhaUsuario.Text = string.Empty;
                    lblSenha.Visible = txtSenhaUsuario.Visible = false;

                    Timer_Inicio.Start();
                    Timer_Limpesa.Start();
                    Timer_Limpesa.Interval = 2000;//2s
                }
                else
                {
                    lblErro.Text = "Senha Incorreta";
                    lblErro.ForeColor = System.Drawing.Color.Red;
                    txtSenhaUsuario.Text = string.Empty;
                    txtSenhaUsuario.Focus();
                }
            }
        }

        private void txtSenhaUsuario_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                txtSenhaUsuario.Text = string.Empty;
                lblSenha.Visible = txtSenhaUsuario.Visible = false;

                Timer_Inicio.Start();
                Timer_Limpesa.Start();
                Timer_Limpesa.Interval = 100; // 1s
            }
        }

        private void tAtualizaListaBios_Tick(object sender, EventArgs e)
        {
            string[] secretarias = System.Configuration.ConfigurationSettings.AppSettings["secretarias"].Split(';');

            int[] idSecretarias = new int[secretarias.Count()];

            for (int i = 0; i < secretarias.Count(); i++)
            {
                idSecretarias[i] = int.Parse(secretarias[i]);
            }

            if (idSecretarias.Count() == 1 && idSecretarias[0] == 0)
            {
                bios = bd.Bios.Where(c => c.Usuario.ATIVO == true).ToList();
            }
            else
            {
                bios = bd.Bios.Where(c => c.Usuario.ATIVO == true && idSecretarias.Contains(c.Usuario.ORGAO_ID.Value)).ToList();
            }

            lblErro.Text = "Digitais Atualizadas :: OK às " + DateTime.Now;
            lblErro.ForeColor = System.Drawing.Color.Blue;
        }

        #endregion

        #region Métodos

        protected void ImpressaoDigital()
        {
            string FpColor = "000000", BkColor = "FFFFFF";
            m_WinOption.WindowStyle = NBioAPI.Type.WINDOW_STYLE.INVISIBLE;

            m_WinOption.Option2.FPForeColor[0] = Convert.ToByte(FpColor.Substring(0, 2), 16);
            m_WinOption.Option2.FPForeColor[1] = Convert.ToByte(FpColor.Substring(2, 2), 16);
            m_WinOption.Option2.FPForeColor[2] = Convert.ToByte(FpColor.Substring(4, 2), 16);

            m_WinOption.Option2.FPBackColor[0] = Convert.ToByte(BkColor.Substring(0, 2), 16);
            m_WinOption.Option2.FPBackColor[1] = Convert.ToByte(BkColor.Substring(2, 2), 16);
            m_WinOption.Option2.FPBackColor[2] = Convert.ToByte(BkColor.Substring(4, 2), 16);

            m_WinOption.FingerWnd = pictureExtWnd.Handle;

            NBioAPI.Type.HFIR FIR;
            m_NBioAPI.OpenDevice(NBioAPI.Type.DEVICE_ID.AUTO);
            m_NBioAPI.Capture(out FIR, NBioAPI.Type.TIMEOUT.DEFAULT, m_WinOption);
            m_NBioAPI.CloseDevice(NBioAPI.Type.DEVICE_ID.AUTO);

        }

        protected void LimparTela()
        {
            pictureExtWnd.Image = null;
            fotoServidor.Image = null;

            lblEntradaManha.Text = string.Empty;
            lblEntradaTarde.Text = string.Empty;
            lblSaidaManha.Text = string.Empty;
            lblSaidaTarde.Text = string.Empty;
            lblMatriculaServidor.Text = string.Empty;

            lblNomeServidor.Text = "Coloque o dedo no leitor.";
            lblErro.Text = string.Empty;
        }

        public void CapturarDigital()
        {
            try
            {
                Timer_Inicio.Stop();
                Timer_Limpesa.Stop();
                objExtraction.Capture(0x01);

                if (objExtraction.ErrorDescription != "NBioAPIERROR_USER_CANCEL")
                {
                    //var bio = bd.Bios.ToList();
                    Usuario usuario = new Usuario();

                    for (int i = 0; i < bios.Count && objExtraction.TextEncodeFIR != null; i++)
                    {
                        objMatching.VerifyMatch(objExtraction.TextEncodeFIR, bios[i].FIR.ToString());
                        if (objMatching.MatchingResult == 1)
                        {
                            usuario = bios[i].Usuario;
                            if (usuario.ATIVO == true)
                            {
                                break;
                            }
                            else
                            {
                                lblErro.Text = "Lamento, mais você está com o estatus de inativo no sistema!";
                                lblErro.ForeColor = System.Drawing.Color.Red;
                                break;
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(usuario.NOME))
                    {
                        BO.IGRPE.LOG(usuario); // Grava independetemente de digitar a senha

                        fotoServidor.Image = BO.IGRPE.CarregarFoto(usuario);
                        lblNomeServidor.Text = usuario.NOME;
                        lblMatriculaServidor.Text = usuario.MATRICULA;

                        txtSenhaUsuario.Text = string.Empty;
                        lblSenha.Visible = txtSenhaUsuario.Visible = true;
                        txtSenhaUsuario.Focus();

                        lblErro.Text = "Aperte a tecla 'ESC' caso não seja você!!\rA confirmação é os quatros  ultimos números de sua matricula!";
                        lblErro.ForeColor = System.Drawing.Color.Blue;

                        CarregaHorarios(usuario);

                    }
                    else
                    {
                        Timer_Inicio.Start();
                        Timer_Limpesa.Start();
                        Timer_Limpesa.Interval = 100;//2s

                        StatusBar.Panels[1].Text = "Servidor não encontrado na lista de servidores ativos.";
                        LimparTela();
                    }
                }
            }
            catch (Exception ex)
            {
                StatusBar.Panels[1].Text = "Erro: " + ex.Message;
                lblErro.Text = ex.Message;
            }

        }// Fim do Método

        public void CarregaHorarios(Usuario usuario)
        {
            bd = new der_db01Entities();

            string data_hoje = BO.IGRPE.GetDate().ToShortDateString();
            Frequencia frequencia = bd.Frequencias.SingleOrDefault(c => c.USUARIO_ID == usuario.USUARIO_ID && c.DATA == data_hoje);

            if (frequencia != null)
            {
                lblEntradaManha.Text = frequencia.ENTRADA_MANHA.ToString();
                lblSaidaManha.Text = frequencia.SAIDA_MANHA.ToString();

                lblEntradaTarde.Text = frequencia.ENTRADA_TARDE.ToString();
                lblSaidaTarde.Text = frequencia.SAIDA_TARDE.ToString();
            }

        }

        #endregion

    }
}
